import csv

def parse_csv(filename):
    mempool = {}
    with open(filename, 'r') as file:
        reader = csv.reader(file)

        for row in reader:                        
            row_items = row[0].split(',')
            txid, fee, weight = row_items[0], row_items[1], row_items[2]
            mempool[txid] = {'fee': int(fee), 'weight': int(weight), 'parents': []}

    return mempool

def select_transactions(mempool):
    block = []
    remaining_weight = 4000000
    sorted_mempool = sorted(mempool.items(), key=lambda x: (-x[1]['fee'], x[1]['weight'], x[0]))
    for txid, tx_info in sorted_mempool:
        if remaining_weight < tx_info['weight']:
            continue
        if all(parent in block for parent in tx_info['parents']):
            block.append(txid)
            remaining_weight -= tx_info['weight']
    return block

def write_block(block):
    with open('block.txt', 'w') as file:
        for txid in block:
            print(txid)
            file.write(txid + '\n')

if __name__ == "__main__":
    mempool = parse_csv('mempool.csv')
    block = select_transactions(mempool)
    write_block(block)
